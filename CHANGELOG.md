## 0.4.0 (2020-09-08)

### New features

- Add filepath and link_type to GitLab client !48
- Add --assets-link flag to support direct asset links !49

### Maintenance

- Deprecate `--assets-links-name` and `--assets-links-url` !49
- Resolve "Do not override Docker latest tag for backports" !46
- Break up CI configuration into includes by stage !45
- Add container scanning to release-cli !39
- Fix secret detection jobs !54
- Use correct dependency-scanning job !56

### Other changes

- Create a release from a tag !51
- Add @pedropombeiro as reviewer !47
- Update changelog generator to accept new labels !44

## 0.3.0 (2020-07-14)

### New features

- Support sending assets to the Releases API !31
- Add asset flags to create command !33
- Add `milestone` and `released_at` to release-cli params !38
- Add `milestones` and `released_at` to the GitLab Client !37

### Bug fixes

- Resolve "Run pipelines on new tags" !30

### Maintenance

- Update `latest` Docker tag !36
- Make name and description optional !40

### Other changes

- Return early if no asset names specified !42

## v0.2.0 (2020-05-21)

### New features

- Build binaries for different OSs !17
- Add danger reviews !11

### Maintenance

- Run jobs `on_success` instead of always !29
- Use security templates for analysis !27
- Update to use mockery:v.1.1.0 !25
- Fix error parsing for releases API !22 (Sashi @ksashikumar)
- Add integration tests with mock server !12

### Documentation changes

- doc: Remove shell markdown for usage blocks !23 (Elan Ruusamäe @glensc)
- Document verisioning process !21

## v0.1.0

- Create Release CLI module [!4](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/4)
- Add GitLab client package [!5](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/5)
- Add command `create` [!6](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/6)
- Add docker image [!13](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/13)
