FROM golang:1.15.4-alpine3.12 AS builder
RUN apk --no-cache add ca-certificates make git && update-ca-certificates

COPY . /release-cli
WORKDIR /release-cli

RUN make build

FROM alpine:3.12

COPY --from=builder /release-cli/bin/release-cli /usr/local/bin/release-cli
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

